# SOFT3888 CP11

## Contents:
* [Introduction](#introduction)
    * [Team Members](#team-members)
* [Extreme Programming](#extreme-programming)
    * [The Planning Game](#1-the-planning-game)
    * [Small Releases](#2-small-releases)
    * [System Metaphor](#3-system-metaphor)
    * [Simple Design](#4-simple-design)
    * [Continuous Testing](#5-continuous-testing)
    * [Refactoring](#6-refactoring)
    * [Pair Programming](#7-pair-programming)
    * [Collective Code Ownership](#8-collective-code-ownership)
    * [Continuous Integration](#9-continuous-integration)
    * [40-Hr Work Week](#10-40-hr-work-week)
    * [On Site Customer](#11-on-site-customer)
    * [Coding Standards](#12-coding-standards)
    * [Working Style](#working-style)

## Introduction
This project is done as part of University of Sydney's INFO3888 Capstone project 2019. 

#### Team Members
* Kae Vern Lee: [klee5710@uni.sydney.edu.au](mailto:klee5710@uni.sydney.edu.au)
* Geoffrey Morgan: [gmor6604@uni.sydney.edu.au](mailto:gmor6604@uni.sydney.edu.au)
* Elbert Wan: [ewan9817@uni.sydney.edu.au](mailto:ewan9817@uni.sydney.edu.au)
* Aditya Pokharel: [apok9890@uni.sydney.edu.au](mailto:apok9890@uni.sydney.edu.au)


## Extreme Programming
In this project, we will be utilising the Extreme Programming (XP) practices. This section outlines a summary of XP itself and the 12 well-known development practices.

Extreme Programming (XP) is defined as a set of values, principles and practices for rapidly developing high-quality software that provides the highest value for the customer in the fastest way possible. It is considered a lightweight methodology, in the sense that it’s a repeatable process for developing software.

XP takes 12 well-known software development practices to their logical extremes. These practices are outlined as follows: 

#### 1. The Planning Game
Business and Development teams cooperate to produce the maximum business value as rapidly as possible:
* The Business side produces a list of desired features for the system (each feature written out as a User Story).
* The Development side estimates the effort required for each story, along with the effort that the team can produce in a given time interval.
* The Business side then decides which stories to implement and in what order, as well as when, and how often, to produce a production release of the system.

#### 2. Small Releases
Release early and often, adding new features each time (start with smallest useful feature).

#### 3. System Metaphor
Each project makes use of an organizing metaphor, which provides an easy to remember naming convention.

#### 4. Simple Design
Use the [simplest possible design](#simplest-design-explanation)* that gets the job done.

#### 5. Continuous Testing
Before programmers add a feature, write a test for it. Two types of tests: Unit tests (test functionality of individual components) and Acceptance tests (test the entire system is functioning as specified by the customer). When all acceptance tests pass for a given user story, that story is considered complete.

#### 6. Refactoring
Refactor out any duplicate code.

#### 7. Pair Programming
All production code is written by 2 programmers on one machine (i.e. code is reviewed as it is written).

#### 8. Collective Code Ownership 
Any developer is expected to be able to work on any part of the codebase at any time.

#### 9. Continuous Integration 
All changes are integrated into the codebase at least daily. The tests have to run 100% both before and after integration.

#### 10. 40-hr Work Week
No overtime, with the possible exception of delivery week. Multiple consecutive weeks of overtime are treated as a sign that something is very wrong with the progress.

#### 11. On-Site Customer
The development team has continuous access to a real live customer (someone who will actually be using the system). For commercial software with lots of customers, a customer proxy (usually a product manager) is used instead.

#### 12. Coding Standards
Everyone codes to the same standards.

#### Working Style
XP teams typically work in a series of fixed iteration cycles. (typically lasting for 1, 2, or 3 weeks).
* At the beginning of each iteration, the team meets with the customer to discuss the features the customer wants done for that iteration. These features are then broken down into individual engineering tasks. Developers sign up for specific tasks, and no developer is allowed to sign up for more work than they completed in the previous iteration.
* For the rest of the iteration the team implements the features that they signed up for, utilizing pair programming for all production code. All code is written test-first (no code is written until there is at least one failing test case).
* At the end of the iteration, the programmers deliver a working system to the customer. The system may not be complete, but all functionality that is implemented works completely, without bugs.
* Actual product release is almost a non-event -> the customer takes the delivered system from some iteration and distributes it to the end users. The only question is: Have the developers added enough functionality to make it worthwhile for the end users to upgrade.

---
##### *Simplest Design Explanation
XP Definition of "simplest" design:
1. The system clearly communicates everything that needs to be communicated at the current instant in its development (i.e. runs every existing test, and that the source code clearly reveals the intention behind it to anyone who reads it).
2. The system contains no duplicate code, unless that would violate (1).
3. The system contains the minimum number of classes possible without violating (1) or (2).
4. The system contains the minimum number of methods possible, consistent with (1), (2) and (3).
