import React, { Component } from "react";
import socketIOClient from "socket.io-client";
import styled from 'styled-components';


const ButtonWrapper = styled.div`
  width: 201px;
  height: 1000px;
  display: flex;
  flex-direction: column;
  user-select: none;
`;

const LeftRightWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const baseButton = styled.div`
  background-color: salmon;
  border: 2px solid grey;
  border-radius: 20px;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
`;

const UpButton = styled(baseButton)`
  width: 200px;
  height: 100px;
`;

const DownButton = styled(UpButton)`
`;

const LeftButton = styled(baseButton)`
  width: 100px;
  height: 100px;
`;

const RightButton = styled(LeftButton)``;

class App extends Component {
  constructor() {
    super();
    this.state = {
      response: false,
      endpoint: "http://andy.local:4001/",
      socket: null,
      error: false,
    };
    this.upAction = this.upAction.bind(this);
    this.downAction = this.downAction.bind(this);
    this.leftAction = this.leftAction.bind(this);
    this.rightAction = this.rightAction.bind(this);

  }

  componentDidMount() {
    const { endpoint } = this.state;    
    const socket = socketIOClient(endpoint, { andysMessage: "hello" });
    this.setState({ socket })
  }

  upAction(type) {
    type === "press"
    ? this.state.socket.emit('upButtonPressed')
    : this.state.socket.emit('upButtonReleased');
  }

  downAction(type) {
    type === "press"
    ? this.state.socket.emit('downButtonPressed')
    : this.state.socket.emit('downButtonReleased');
  }

  leftAction(type) {
    type === "press"
    ? this.state.socket.emit('leftButtonPressed')
    : this.state.socket.emit('leftButtonReleased');
  }

  rightAction(type) {
    type === "press"
    ? this.state.socket.emit('rightButtonPressed')
    : this.state.socket.emit('rightButtonReleased');
  }

render() {
    const { response, error } = this.state;
    return (
      <div style={{ textAlign: "center" }}>
        <ButtonWrapper>
          <UpButton
            onTouchStart={() => this.upAction("press")}
            onTouchEnd={() => this.upAction("release")}
            onMouseDown={() => this.upAction("press")}
            onMouseUp={() => this.upAction("release")}
            >U</UpButton>
          <LeftRightWrapper>
            <LeftButton
              onTouchStart={() => this.leftAction("press")}
              onTouchEnd={() => this.leftAction("release")}
              onMouseDown={() => this.leftAction("press")}
              onMouseUp={() => this.leftAction("release")}
            >L</LeftButton>
            <RightButton
              onTouchStart={() => this.rightAction("press")}
              onTouchEnd={() => this.rightAction("release")}
              onMouseDown={() => this.rightAction("press")}
              onMouseUp={() => this.rightAction("release")}
            >R</RightButton>
          </LeftRightWrapper>
          <DownButton
            onTouchStart={() => this.downAction("press")}
            onTouchEnd={() => this.downAction("release")}
            onMouseDown={() => this.downAction("press")}
            onMouseUp={() => this.downAction("release")}
          >D</DownButton>
        </ButtonWrapper>
      </div>
    );
  }
}
export default App;
