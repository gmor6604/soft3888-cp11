﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class CubeController : SocketIOComponent
{
    private float _xRotation = 0f;

    private float _yRotation = 0f;

    private float _zRotation = 0f;
    
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        SetupSocketEvents();
    }
    
    

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        transform.rotation = Quaternion.Euler(_xRotation, _yRotation, _zRotation);
    }

    private void SetupSocketEvents()
    {
        On("xRotation", (E) =>
        {
            var xRotation = 0.0f;
            float.TryParse(E.data["amount"].ToString(), out _xRotation);
//            _xRotation += xRotation;
        });
        On("yRotation", (E) =>
        {
            var yRotation = 0.0f;
            float.TryParse(E.data["amount"].ToString(), out _yRotation);
//            _yRotation += yRotation;
        });
        On("zRotation", (E) =>
        {
            var zRotation = 0.0f;
            float.TryParse(E.data["amount"].ToString(), out _zRotation);
//            _zRotation += zRotation;
        });
//        Console.Write(_xRotation);
//        Console.Write("##");
//        Console.Write(_yRotation);
//        Console.Write("##");
//        Console.Write(_zRotation);
//        Console.WriteLine();

    }
    
    
}
