const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const get = require('lodash/get');
const axios = require("axios");
const port = process.env.PORT || 4001;
const index = require("./route/index");
const app = express();
app.use(index);
const server = http.createServer(app);
const io = socketIo(server);

let unitySocket = null;
let controllerSocket = null;
console.log("Begin");

const socketCommands = {
  // BUTTON PRESSED
  upPressed: 'upButtonPressed',
  downPressed: 'downButtonPressed',
  leftPressed: 'leftButtonPressed',
  rightPressed: 'rightButtonPressed',

  //BUTTON RELEASED
  upReleased: 'upButtonReleased',
  downReleased: 'downButtonReleased',
  leftReleased: 'leftButtonReleased',
  rightReleased: 'rightButtonReleased',
};


const root = io
  .on('connection', (socket) => {
    unitySocket = socket;
    const isUnity = get(socket, 'handshake.url', '').includes('unity');

    if (isUnity) {
      unitySocket = socket;
      console.log("Unity connected")
    }
    else  {
      controllerSocket = socket;
      console.log("ROOT Controller connected");
    }
    socket.on('disconnect', () => {
      if (isUnity) console.log("Unity disconnected");
      else console.log('Root disconnected');
    });

    Object.values(socketCommands).forEach((command) => {
      socket.on(command, () => {
        console.log(`command: ${command}`);
        socket.broadcast.emit(command);
      });
    });
  });


server.listen(port, () => console.log(`Listening on port ${port}`));
